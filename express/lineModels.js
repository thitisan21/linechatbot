const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const NoteSchema = Schema(
    {
        message: {
            id: { type: String },
            text: { type: String },
            type: { type: String }
        },
        model: String,
        replyToken: String,
        type: String,
        source: {
            userId: { type: String },
            type: { type: String }
        },
        timestamp: Date,
    }
);

const lineModels = mongoose.model("line", NoteSchema);

module.exports = lineModels;