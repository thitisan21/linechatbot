const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const NoteSchema = Schema(
    {
        date: Date,
        lottoNumber: String,
        typeLotto: String,
        reward: Number
    }
);

module.exports = mongoose.model("lotto", NoteSchema);
;