const express = require('express');
const app = express();
const cors = require('cors')
const path = require('path');
const bodyParser = require('body-parser');
const serverless = require('serverless-http');
const router = express.Router();
const mongoose = require('mongoose');
const lottoModels = require('./lottoModels');
const request = require('request-promise');
mongoose.connect(
    'mongodb+srv://admin:j4GXgToVUJxrQUhG@cluster0-k2drd.mongodb.net/demolotto?retryWrites=true&w=majority',
    { useNewUrlParser: true }
);
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

// router.post('/find/lotto', (req, res) => {
//     return new Promise((reslove, reject) => {
//         lottoModels.findOne({ lottoNumber: req.body.lottoNumber }, (err, result) => {
//             if (!err) {
//                 if (result == null) {
//                     lottoModels.find({}, (err, resultInside) => {
//                         const win = resultInside.filter(item => {
//                             if (item.reward === 4000 || item.reward === 2000) {
//                                 if (req.body.lottoNumber.substring(0, 3) === item.lottoNumber) {
//                                     return item
//                                 } else if (req.body.lottoNumber.substring(3, 6) === item.lottoNumber) {
//                                     return item
//                                 } else if (req.body.lottoNumber.substring(4, 6) === item.lottoNumber) {
//                                     return item
//                                 }
//                             }
//                         })
//                         res.json({ win })
//                     })
//                 } else {
//                     lottoModels.find({}, (err, resultInside) => {
//                         const win = resultInside.filter(item => {
//                             if (item.reward === 4000 || item.reward === 2000) {
//                                 if (req.body.lottoNumber.substring(0, 3) === item.lottoNumber) {
//                                     return item
//                                 } else if (req.body.lottoNumber.substring(3, 6) === item.lottoNumber) {
//                                     return item
//                                 } else if (req.body.lottoNumber.substring(4, 6) === item.lottoNumber) {
//                                     return item
//                                 }
//                             }
//                         })
//                         res.json({ result, win })
//                     })
//                 }
//             }
//         });
//         reslove()
//     })
// })
router.post('/create/lotto', (req, res) => {
    return new Promise((reslove, reject) => {
        lottoModels.create(req.body, (err) => {
            if (!err) {
                res.json({ status: true })
            }
        });
        reslove()
    })
})
router.post('/find/lotto', (req, res) => {
    return new Promise((reslove, reject) => {
        request.get("https://lotto.api.rayriffy.com/latest", (err, ress, body) => {
            var rewardLasted = JSON.parse(body)
            var arr = []
            //console.log(rewardLasted.response.runningNumbers)
            var result = rewardLasted.response.prizes.filter(item => {
                return item.number = item.number.find(element => element === req.body.lottoNumber)
            })
            var win = rewardLasted.response.runningNumbers.filter(item => {
                return item.number = item.number.find(element => {
                    if (req.body.lottoNumber.substring(0, 3) === element) {
                        return item
                    } else if (req.body.lottoNumber.substring(3, 6) === element) {
                        return item
                    } else if (req.body.lottoNumber.substring(4, 6) === element) {
                        return item
                    }
                })
            })
            res.json({ result: result, win: win })
        })
        reslove()
    })
})
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use('/.netlify/functions/lotto', router);  // path must route to lambda
app.use('/', (req, res) => res.sendFile(path.join(__dirname, '../index.html')));

module.exports = app;
module.exports.handler = serverless(app);
