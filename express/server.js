'use strict';
//https://dialogflow.cloud.google.com/v1/integrations/line/webhook/e4cb2e67-a35b-41d3-885f-47299de3a07b
//https://focused-hamilton-c12077.netlify.com/.netlify/functions/server/webhook
const express = require('express');
const path = require('path');
const serverless = require('serverless-http');
const app = express();
const cors = require('cors')
const bodyParser = require('body-parser');
const request = require('request-promise');
const fetch = require('node-fetch')
const { WebhookClient } = require('dialogflow-fulfillment');
const router = express.Router();
const LINE_MESSAGING_API = 'https://api.line.me/v2/bot/message/reply';
const LINE_HEADER = {
  'Content-Type': 'application/json',
  'Authorization': "Bearer 2s2JNxpeis3fWrUbkN6QI9NqeXhWty+X0ausA8fuHnWV6cAzvrOnVa46AXNirF4aHyuXdl0mQm+QgwCDSA5kS5q71ZbnogZyYqrOJpF2O9vC79sKw5b7Rmm/ww3fLG5JQ9lCE1p0+EF6eAbH3zyxCwdB04t89/1O/w1cDnyilFU="
};
const admin = require("firebase-admin");
var serviceAccount = require("../testlinechatbot-36695-firebase-adminsdk-w7xse-93e292c297.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://testlinechatbot-36695.firebaseio.com"
});
const mongoose = require('mongoose');
const lineModels = require('./lineModels');

mongoose.connect(
  'mongodb+srv://admin:j4GXgToVUJxrQUhG@cluster0-k2drd.mongodb.net/test?retryWrites=true&w=majority',
  { useNewUrlParser: true }
);
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

router.post('/webhook', (req, res) => {
  return new Promise((reslove, reject) => {
    //console.log(postToDB(req))
    if (req.method === "POST") {
      console.log("inside /webhook")
      console.log(req.body.events[0])
      let event = req.body.events[0]
      postToDB(req)
      if (event.type === "message" && event.message.type === "text") {
        postToDialogflow(req);
      } else {
        reply(req);
      }
    }
    reslove()
  })
})

const reply = req => {
  return new Promise(async (reslove, reject) => {
    request.post({
      uri: `${LINE_MESSAGING_API}`,
      headers: LINE_HEADER,
      body: JSON.stringify({
        replyToken: req.body.events[0].replyToken,
        messages: [
          {
            type: "text",
            text: JSON.stringify(req.body)
          }
        ]
      })
    });
    reslove()
  })
};

const postToDialogflow = req => {
  return new Promise(async (reslove, reject) => {
    req.headers.host = "bots.dialogflow.com";
    request.post({
      uri: "https://bots.dialogflow.com/line/e4cb2e67-a35b-41d3-885f-47299de3a07b/webhook",
      headers: req.headers,
      body: JSON.stringify(req.body)
    });
    reslove()
  })
};

router.post('/searchDB', (req, res) => {
  return new Promise((reslove, reject) => {
    console.log("inside /searchDB")
    console.log(req.body)
    console.log(req.body.originalDetectIntentRequest.payload.data.source)
    console.log(req.body.originalDetectIntentRequest.payload.data.message)
    var query = {}
    query.type = req.body.originalDetectIntentRequest.payload.data.message.type
    query.replyToken = req.body.originalDetectIntentRequest.payload.data.replyToken
    query.timestamp = req.body.originalDetectIntentRequest.payload.data.timestamp
    query.source = req.body.originalDetectIntentRequest.payload.data.source
    query.message = req.body.originalDetectIntentRequest.payload.data.message
    query.lineBot = "Bot"
    postToDBWithFF(query)
    // console.log(req.body.queryResult.fulfillmentMessages[0])
    // console.log(req.body.queryResult.fulfillmentMessages[0].text)
    const agent = new WebhookClient({
      request: req,
      response: res
    });
    //connectFirebase("s")
    // function connectFirebase(agent) {
    //   return new Promise((reslove, reject) => {
    //     //console.log(agent.originalRequest)
    //     var id = req.body.queryResult.parameters.id.toString();
    //     //var id = req.body.id.toString();
    //     request({
    //       method: 'GET',
    //       uri: `https://testlinechatbot-36695.firebaseio.com/users.json`,
    //     }, (err, req, result) => {
    //       if (!err) {
    //         var data = JSON.parse(result)
    //         const findData = data.find(item =>
    //           item.username === id
    //         )
    //         var msg = "ชื่อ: " + findData.name + " รหัสนิสิต: " + findData.username
    //         agent.add(msg);
    //         reslove();
    //       } else {
    //         console.log(err)
    //       }
    //     })
    //   })
    // }
    var intentMap = new Map();
    intentMap.set("people - custom - yes", connectFirebase);
    agent.handleRequest(intentMap);
    reslove()
  })
})

const postToDB = req => {
  lineModels.create(req.body.events[0], (err) => {
    if (!err) {
      var query = { status: true }
      return query
    }
  });
}
const postToDBWithFF = req => {
  lineModels.create(req, (err) => {
    if (!err) {
      var query = { status: true }
      return query
    }
  });
}

router.post('/login', (req, res) => {
  return new Promise(async (reslove, reject) => {
    request({
      method: 'POST',
      uri: `https://buu-api.buu.ac.th/api/version1/authBuu`,
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(req.body)
    }, (err, req, result) => {
      if (!err) {
        console.log(JSON.parse(result))
        res.send(JSON.parse(result))
        reslove();
      } else {
        console.log(err)
      }
    })

    // fetch('https://buu-api.buu.ac.th/api/version1/authBuu', {
    //   method: 'POST',
    //   mode: 'cors',
    //   cache: 'no-cache',
    //   credentials: 'include',
    //   headers: {
    //     'Content-Type': 'application/json'
    //   },
    //   redirect: 'follow',
    //   referrerPolicy: 'no-referrer',
    //   body: JSON.stringify(req.body)
    // }).then(data => {
    //   data.json().then(item => {
    //     res.json(item)
    //     reslove();
    //   })
    // })
  })
})
router.post('/createL', (req, res) => {

})
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use('/.netlify/functions/server', router);  // path must route to lambda
app.use('/', (req, res) => res.sendFile(path.join(__dirname, '../index.html')));

module.exports = app;
module.exports.handler = serverless(app);
